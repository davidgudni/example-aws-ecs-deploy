# Example using the aws-ecs-deploy pipe to deploy to AWS Elastic Container Service
## How to use this repo

1. To start you need to fork a repo.
2. You'll also need and AWS account with an [ECS service](https://aws.amazon.com/ecs/) set up and running.
3. An AWS access key with permissions associated to execute the RegisterTaskDefinition and UpdateService actions.
4. Enable Pipelines in your repo and congirue the [repository variables](https://confluence.atlassian.com/bitbucket/variables-in-pipelines-794502608.html#Variablesinpipelines-Repositoryvariables)
5. You can keep the ECS_CLUSTER_NAME, ECS_SERVICE_NAME and ECS_TASK_FAMILY_NAME as is. In this case you should have a cluster, service and task definition with the corresponding names in ECS. You can also replace them with your own values.

I am doing a change

More changes
